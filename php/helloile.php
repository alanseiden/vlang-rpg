<?php
$ile = extension_loaded ('ile');
if (!$ile) {
  echo "ile extension not loaded.\n";
}

$meChar100 = "hi from php";
$meFloat8 = 5.6;
$meChar50 = "zend server";
$meInt = 8;
$mePacked = 11.11;
$meZoned = 22.22;
echo "before ILE call ... $meChar100,$meFloat8,$meChar50,$meInt,$mePacked,$meZoned\n";
ile_callback("IleCallMe",$meChar100,$meFloat8,$meChar50,$meInt,$mePacked,$meZoned);
echo "after ILE call ... $meChar100,$meFloat8,$meChar50,$meInt,$mePacked,$meZoned\n";

?>
